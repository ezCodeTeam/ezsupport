package ez.code.bukkit.core.bukkit.utils;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.ChatClickable;
import net.minecraft.server.v1_8_R3.ChatClickable.EnumClickAction;
import net.minecraft.server.v1_8_R3.ChatHoverable;
import net.minecraft.server.v1_8_R3.ChatHoverable.EnumHoverAction;
import net.minecraft.server.v1_8_R3.ChatMessage;
import net.minecraft.server.v1_8_R3.ChatModifier;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import net.minecraft.server.v1_8_R3.PlayerList;

public class EzChat {

	public static void sendMessageWithCommand(String playerName, String message, String command, String tooltip) {
		IChatBaseComponent base = new ChatMessage(message, new Object[0]);
		base.setChatModifier(new ChatModifier());
		base.getChatModifier().setChatClickable(new ChatClickable(EnumClickAction.RUN_COMMAND, command));
		base.getChatModifier().setChatHoverable(
				new ChatHoverable(EnumHoverAction.SHOW_TEXT, new ChatMessage(tooltip, new Object[0])));
		PlayerList list = MinecraftServer.getServer().getPlayerList();
		list.getPlayer(playerName).sendMessage(base);
	}

	public static void sendMessageWithCommand(String playerName, String message, String command) {
		IChatBaseComponent base = new ChatMessage(message, new Object[0]);
		base.setChatModifier(new ChatModifier());
		base.getChatModifier().setChatClickable(new ChatClickable(EnumClickAction.RUN_COMMAND, command));
		PlayerList list = MinecraftServer.getServer().getPlayerList();
		list.getPlayer(playerName).sendMessage(base);
	}

	public static void sendMessageWithSuggestCommand(String playerName, String message, String command,
			String tooltip) {
		IChatBaseComponent base = new ChatMessage(message, new Object[0]);
		base.setChatModifier(new ChatModifier());
		base.getChatModifier().setChatClickable(new ChatClickable(EnumClickAction.SUGGEST_COMMAND, command));
		base.getChatModifier().setChatHoverable(
				new ChatHoverable(EnumHoverAction.SHOW_TEXT, new ChatMessage(tooltip, new Object[0])));
		PlayerList list = MinecraftServer.getServer().getPlayerList();
		list.getPlayer(playerName).sendMessage(base);
	}

	public static void sendMessageWithSuggestCommand(String playerName, String message, String command) {
		IChatBaseComponent base = new ChatMessage(message, new Object[0]);
		base.setChatModifier(new ChatModifier());
		base.getChatModifier().setChatClickable(new ChatClickable(EnumClickAction.SUGGEST_COMMAND, command));
		PlayerList list = MinecraftServer.getServer().getPlayerList();
		list.getPlayer(playerName).sendMessage(base);
	}

	public static void sendMessageWithLink(String playerName, String message, String link, String tooltip) {
		IChatBaseComponent base = new ChatMessage(message, new Object[0]);
		base.setChatModifier(new ChatModifier());
		base.getChatModifier().setChatClickable(new ChatClickable(EnumClickAction.OPEN_URL, link));
		base.getChatModifier().setChatHoverable(
				new ChatHoverable(EnumHoverAction.SHOW_TEXT, new ChatMessage(tooltip, new Object[0])));
		PlayerList list = MinecraftServer.getServer().getPlayerList();
		list.getPlayer(playerName).sendMessage(base);
	}

	public static void sendMessageWithLink(String playerName, String message, String link) {
		IChatBaseComponent base = new ChatMessage(message, new Object[0]);
		base.setChatModifier(new ChatModifier());
		base.getChatModifier().setChatClickable(new ChatClickable(EnumClickAction.OPEN_URL, link));
		PlayerList list = MinecraftServer.getServer().getPlayerList();
		list.getPlayer(playerName).sendMessage(base);
	}

	public static String colorMessage(String message, char replacer) {
		message = ChatColor.translateAlternateColorCodes(replacer, message);
		return message;
	}

	public static String colorMessage(String message) {
		message = ChatColor.translateAlternateColorCodes('&', message);
		return message;
	}
}
