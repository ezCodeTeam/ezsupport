package ez.code.bukkit.core.bukkit.utils;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;

/**
 * 
 * @author MrPowerGamerBR
 *         <p>
 *         SPARKLYPOWER.NET
 *         </p>
 *
 */
public abstract class EzCommand implements CommandExecutor {

	private String command, usage, description, permission, permissionMessage, plugin;
	private List<String> aliases;
	private static CommandMap commandMap;

	public EzCommand(String command) {
		this(command, null, null, null, null, null);
	}

	public EzCommand(String command, String usage) {
		this(command, usage, null, null, null, null);
	}

	public EzCommand(String command, String usage, String description) {
		this(command, usage, description, null, null, null);
	}

	public EzCommand(String command, String usage, String description, String permission) {
		this(command, usage, description, permission, null, null);
	}

	public EzCommand(String command, String usage, String description, String permission, String permissionMessage) {
		this(command, usage, description, permission, permissionMessage, null);
	}

	public EzCommand(String command, String usage, String description, String permission, String permissionMessage,
			List<String> aliases) {
		this(command, usage, description, permission, permissionMessage, aliases, null);

	}

	public EzCommand(String command, String usage, String description, String permission, String permissionMessage,
			List<String> aliases, String plugin) {
		this.command = command;
		this.usage = usage;
		this.description = description;
		this.permission = permission;
		this.permissionMessage = permissionMessage;
		this.aliases = aliases;
		this.plugin = plugin;
		register();
	}

	private void register() {
		EzReflectCommand cmd = new EzReflectCommand(command, this);
		if (usage != null)
			cmd.setAliases(aliases);
		if (description != null)
			cmd.setDescription(description);
		if (permission != null)
			cmd.setPermission(permission);
		if (permissionMessage != null)
			cmd.setPermissionMessage(permissionMessage);
		if (plugin != null)
			getCommandMap().register(plugin, cmd);
		else
			getCommandMap().register("", cmd);

	}

	private CommandMap getCommandMap() {
		if (commandMap == null) {
			try {
				Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap");
				field.setAccessible(true);
				commandMap = (CommandMap) field.get(Bukkit.getServer());
				return getCommandMap();
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
				return getCommandMap();
			}
		} else {
			return commandMap;
		}
	}

	@Override
	public abstract boolean onCommand(CommandSender s, Command cmd, String lbl, String[] args);

	private class EzReflectCommand extends Command {

		EzCommand exe;

		protected EzReflectCommand(String command, EzCommand exe) {
			super(command);
			this.exe = exe;
		}

		@Override
		public boolean execute(CommandSender s, String lbl, String[] args) {
			return exe.onCommand(s, this, lbl, args);
		}

	}
}
