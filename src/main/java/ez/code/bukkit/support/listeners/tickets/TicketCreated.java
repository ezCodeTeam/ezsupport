package ez.code.bukkit.support.listeners.tickets;

import org.bukkit.Bukkit;

import ez.code.bukkit.support.EzSupport;
import ez.code.bukkit.support.managers.ticket.Ticket;
import ez.code.bukkit.support.managers.ticket.listener.TicketListener;
import net.md_5.bungee.api.ChatColor;

public class TicketCreated implements TicketListener {

	@Override
	public void run(Ticket ticket) {
		Bukkit.broadcast(
				ChatColor.translateAlternateColorCodes('&',
						EzSupport.getPrefix() + "&aA new ticket created by " + ticket.getTicketPlayer().getName()),
				"ezsupport.staff");
	}
}
