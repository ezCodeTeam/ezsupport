package ez.code.bukkit.support.listeners.tickets;

import static ez.code.bukkit.support.EzSupport.talkToSender;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import ez.code.bukkit.support.managers.ticket.Ticket;
import ez.code.bukkit.support.managers.ticket.listener.TicketListener;

public class TicketAnswered implements TicketListener {

	@Override
	public void run(Ticket ticket) {
		Player p = Bukkit.getPlayer(ticket.getTicketPlayer().getName());
		if (p != null) {
			talkToSender(p, "&aYour ticket has been answere! Use \"/ticket\" to see!");
		}
	}

}
