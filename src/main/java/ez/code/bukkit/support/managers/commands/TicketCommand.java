
package ez.code.bukkit.support.managers.commands;

import static ez.code.bukkit.support.EzSupport.talkToSender;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ez.code.bukkit.core.bukkit.utils.EzCommand;
import ez.code.bukkit.support.EzSupport;
import ez.code.bukkit.support.managers.ticket.Ticket;
import ez.code.bukkit.support.managers.ticket.TicketRate;

public class TicketCommand extends EzCommand {

	public TicketCommand() {
		super("ticket", // command
				null, // usage
				"Command to create and manage tickets", // description
				null, // permission
				null, // permission message
				Arrays.asList("help", "ajuda"), // aliases
				EzSupport.getPluginName() // command register. eg:
											// /minecraft:tp (minecraft = register | tp = command)
		);
	}

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String lbl, String[] args) {
		if (!(s instanceof Player)) {
			talkToSender(s, "&cOnly players can use this command");
			return false;
		}
		if (EzSupport.getInstance().getPlayerManager().getPlayer(s.getName()).hasPendingTickets()) {
			Ticket ticket = EzSupport.getInstance().getPlayerManager().getPlayer(s.getName()).getPendingTickets(1)
					.get(0);
			if (args.length > 0) {
				try {
					byte b = Byte.parseByte(args[0]);
					if (b < 1 || b > 5) {
						talkToSender(s, "&cInvalid syntax! Use \"/ticket [1 ~ 5]\".");
						return false;
					}
					ticket.setRate(new TicketRate((byte) (Byte.parseByte(args[0]) - 1)));
					talkToSender(s, "Ty!");
					return true;
				} catch (NumberFormatException e) {
					talkToSender(s, "&cInvalid syntax! Use \"/ticket [1 ~ 5]\".");
					return false;
				}
			} else {
				talkToSender(s, Arrays.asList( //
						"&7--==[&bTicket " + ticket.getTicketId() + "&7]==--", // HEADER
						"Your question: &b" + ticket.getQuestion(), // QUESTION
						"Answer: &b" + ticket.getAnswer(), // ANSWER
						"Answer by: &b" + ticket.getTicketStaffer().getName(), // ANSWER BY
						"Answer in: &b" + ticket.getAnswerInDate(), // ANSWER IN
						"Solved? &bUse /ticket [1 ~ 5]&7 to rate the support.", // SOLVED?
						"Need more help? &bUse /ticket [another question]" // MORE HELP
				), "&7");
			}
			return true;
		} else if (EzSupport.getInstance().getPlayerManager().getPlayer(s.getName()).hasOpenedTickets()) {
			talkToSender(s, "&aYou've a opened ticket! Wait for an answer!");
			return true;
		} else {
			if (args.length > 0) {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < args.length; i++) {
					sb.append(args[i]);
					if (i != (args.length - 1)) {
						sb.append(" ");
					}
				}
				EzSupport.getInstance().getTicketManager().createTicket(
						EzSupport.getInstance().getPlayerManager().getPlayer(s.getName()).getId(),
						Bukkit.getServerName(), sb.toString());
				talkToSender(s, "&aTicket sended! Wait for an answer!");
				return true;
			} else {
				talkToSender(s, "&cWrong syntax! Use \"/ticket [question]\"&c.");
				return false;
			}
		}
	}

}
