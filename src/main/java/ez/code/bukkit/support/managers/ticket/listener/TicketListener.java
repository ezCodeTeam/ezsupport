package ez.code.bukkit.support.managers.ticket.listener;

import ez.code.bukkit.support.managers.ticket.Ticket;

public interface TicketListener {

	public void run(Ticket ticket);

}
