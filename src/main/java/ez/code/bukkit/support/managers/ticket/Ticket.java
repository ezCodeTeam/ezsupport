package ez.code.bukkit.support.managers.ticket;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ez.code.bukkit.support.EzSupport;
import ez.code.bukkit.support.managers.player.TicketPlayer;
import ez.code.bukkit.support.managers.ticket.listener.TicketListener;
import ez.code.core.utils.sql.EzSQL;

public class Ticket {

	private int ticketId;
	private int playerId;
	private String question;
	private int createIn;
	private int stafferId;
	private String answer;
	private int answerIn;
	private TicketRate rate;
	private List<Integer> views;
	private List<Integer> messages;

	private EzSQL sql = EzSupport.getInstance().getSQL();

	public Ticket(int ticketId) {
		this.ticketId = ticketId;
		reload();
	}

	public Ticket reload() {
		try (ResultSet rs = sql.executeQuery("SELECT * FROM tickets WHERE ticketId = '" + ticketId + "';")) {
			if (rs.next()) {
				// GETTING NOT NULL VALUES
				ticketId = rs.getInt("ticketId");
				playerId = rs.getInt("playerId");
				question = rs.getString("question");
				createIn = rs.getInt("createIn");
				// GETTING VALUES THAT CAN BE NULL
				stafferId = rs.getInt("stafferId");
				answer = rs.getString("answer");
				answerIn = rs.getInt("answerIn");
				rate = new TicketRate(rs.getByte("rate"), rs.getString("rateMessage"));
			}
			try (ResultSet rsViews = sql
					.executeQuery("SELECT playerId FROM views WHERE ticketId = '" + ticketId + "';")) {
				views = new ArrayList<Integer>();
				while (rsViews.next()) {
					views.add(rs.getInt("playerId"));
				}
			}
			try (ResultSet rsMessages = sql
					.executeQuery("SELECT messageId FROM messages WHERE ticketId = '" + ticketId + "';")) {
				messages = new ArrayList<Integer>();
				while (rsMessages.next()) {
					views.add(rs.getInt("messageId"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	// GETTERS

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public int getPlayerId() {
		return playerId;
	}

	public TicketPlayer getTicketPlayer() {
		return EzSupport.getInstance().getPlayerManager().getPlayer(getPlayerId());
	}

	public TicketPlayer getTicketStaffer() {
		return EzSupport.getInstance().getPlayerManager().getPlayer(getStafferId());
	}

	public ArrayList<String> getViewsPlayerNames() {
		ArrayList<String> list = new ArrayList<String>();
		for (int i : getViews()) {
			if (!list.contains(EzSupport.getInstance().getPlayerManager().getPlayer(i).getName()))
				list.add(EzSupport.getInstance().getPlayerManager().getPlayer(i).getName());
		}
		return list;
	}

	public boolean exists() {
		return (question != null);
	}

	public String getQuestion() {
		return question;
	}

	public int getCreateIn() {
		return createIn;
	}

	public Date getCreateInDate() {
		return EzSupport.getInstance().getTicketManager().fromSeconds(getCreateIn());
	}

	public int getStafferId() {
		return stafferId;
	}

	public String getAnswer() {
		return answer;
	}

	public int getAnswerIn() {
		return answerIn;
	}

	public Date getAnswerInDate() {
		return EzSupport.getInstance().getTicketManager().fromSeconds(getAnswerIn());
	}

	public TicketRate getRate() {
		return rate;
	}
	
	public List<Integer> getMessages(){
		return messages;
	}

	public String getRateMessage() {
		return rate.getMessage();
	}

	public List<Integer> getViews() {
		return views;
	}

	// SETTERS
	public Ticket setQuestion(String question) {
		try {
			this.question = question;
			sql.executeUpdate("UPDATE tickets SET question = '" + question.replace("'", "''") + "' WHERE ticketId = '"
					+ ticketId + "';");
			// reload();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public Ticket setCreateIn(int createIn) {
		try {
			this.createIn = createIn;
			sql.executeUpdate("UPDATE tickets SET createIn = '" + createIn + "' WHERE ticketId = '" + ticketId + "';");
			// reload();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public Ticket setStafferId(int stafferId) {
		try {
			this.stafferId = stafferId;
			sql.executeUpdate(
					"UPDATE tickets SET stafferId = '" + stafferId + "' WHERE ticketId = '" + ticketId + "';");
			// reload();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public Ticket setAnswer(String answer) {
		try {
			this.answer = answer;
			sql.executeUpdate("UPDATE tickets SET answer = '" + answer.replace("\'", "''") + "' WHERE ticketId = '"
					+ ticketId + "';");
			reload();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public Ticket setAnswerIn(int answerIn) {
		try {
			this.answerIn = answerIn;
			sql.executeUpdate("UPDATE tickets SET answerIn = '" + answerIn + "' WHERE ticketId = '" + ticketId + "';");
			// reload();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public Ticket setRate(TicketRate rate) {
		try {
			this.rate = rate;
			sql.executeUpdate("UPDATE tickets SET rate = '" + rate.getLevel().getValue() + "' WHERE ticketId = '"
					+ ticketId + "';");
			// reload();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public Ticket setRateMessage(String rateMessage) {
		try {
			this.rate = new TicketRate(rate.getLevel(), rateMessage);
			sql.executeUpdate("UPDATE tickets SET rateMessage = '" + rateMessage.replace("'", "''")
					+ "' WHERE ticketId = '" + ticketId + "';");
			// reload();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	// SPECIAL METHODS

	public Ticket addView(int playerId) {
		System.out.println("views = " + getViews());
		if (!getViews().contains(playerId)) {
			System.out.println("views don't contais " + playerId);
			try {
				sql.executeUpdate(
						"INSERT INTO views (ticketId, playerId) values ('" + ticketId + "', '" + playerId + "');");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("views contais " + playerId);
		}
		reload();
		return this;
	}

	public boolean isAnswered() {
		return answer != null;
	}

	public Ticket answer(int stafferId, String answer, boolean force) {
		if (!isAnswered() || force) {
			setAnswer(answer);
			setStafferId(stafferId);
			setAnswerIn(EzSupport.getInstance().getTicketManager().getTimeInSeconds());
			for (TicketListener l : EzSupport.getInstance().getTicketManager().getListernersManager()
					.getAnswerTicketListeners()) {
				l.run(this);
			}
		}
		reload();
		return this;
	}

	public TicketStatus getTicketStatus() {
		if (!isAnswered())
			return TicketStatus.OPENED;
		if (isSpam())
			return TicketStatus.SPAM;
		if (isRated())
			return TicketStatus.CLOSED;
		else
			return TicketStatus.PENDING;

	}

	public String getTicketStatusFormated() {
		switch (getTicketStatus()) {
		case OPENED:
			return "&2&lOpen";
		case CLOSED:
			return "&4&lClosed";
		case PENDING:
			return "&e&lPending";
		case SPAM:
			return "&c&lSPAM";
		}
		return null;
	}

	public boolean isRated() {
		return (rate.getLevel() != null);
	}

	public boolean hasRateMessage() {
		return (rate.getMessage() != null);
	}

	public Ticket markAsSpam(int stafferId, boolean force) {
		if (!isAnswered() || force) {
			setAnswer("SPAM");
			setStafferId(stafferId);
			// IDEA addView()?
		}
		reload();
		return this;
	}

	public boolean isSpam() {
		return (isAnswered() && answer.equals("SPAM"));
	}

}
