package ez.code.bukkit.support.managers.ticket;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import ez.code.bukkit.support.EzSupport;
import ez.code.bukkit.support.managers.ticket.listener.TicketListener;
import ez.code.bukkit.support.managers.ticket.listener.TicketListernesManager;
import ez.code.core.utils.sql.EzSQL;

public class TicketManager {

	private final EzSQL sql = EzSupport.getInstance().getSQL();
	private LoadingCache<Integer, Ticket> cache;
	private final TicketListernesManager listernes;

	public TicketManager() {
		clearCache();
		listernes = new TicketListernesManager();
	}

	public TicketListernesManager getListernersManager() {
		return listernes;
	}

	public void clearCache() {
		cache = CacheBuilder.newBuilder().maximumSize(1000).expireAfterAccess(5, TimeUnit.MINUTES)
				.build(new CacheLoader<Integer, Ticket>() {
					@Override
					public Ticket load(Integer id) throws Exception {
						return new Ticket(id);
					}
				});
	}

	public Ticket getTicket(int id) {
		try {
			return cache.get(id);
		} catch (ExecutionException e) {
			e.printStackTrace();
			return null;
		}
	}

	// d MITH OF THIS CODE IS WINX64
	public synchronized Ticket createTicket(int playerId, String server, String question, int ticketExtends) {
		try {
			// FIXME improve
			if (ticketExtends == 0) {
				sql.executeUpdate("INSERT INTO tickets (playerId, server, question, createIn) values ('" + playerId
						+ "', '" + server.replace("'", "''") + "', '" + question.replace("'", "''") + "', '"
						+ getTimeInSeconds() + "');");
			} else {
				sql.executeUpdate("INSERT INTO tickets (playerId, server, question, extended) values ('" + playerId
						+ "', '" + server.replace("'", "''") + "', '" + question.replace("'", "''") + "', '" + ticketExtends
						+ "');");
			}
			Ticket ticket = getTicket(sql.executeQuery("SELECT last_insert_rowid();").getInt(1)).reload();
			cache.put(ticket.getTicketId(), ticket);
			for (TicketListener l : getListernersManager().getCreateTicketListeners()) {
				l.run(ticket);
			}
			return ticket;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	// d MITH OF THIS CODE IS WINX64
	public Ticket createTicket(int playerId, String server, String question) {
		// FIXME improve
		return createTicket(playerId, server, question, 0);
	}

	public ArrayList<Ticket> getOpenedTickets(int limit, TicketOrder order) {
		if (order == TicketOrder.OLD) {
			// TODO cache this
			try (ResultSet rs = sql.executeQuery(
					"SELECT ticketId FROM tickets WHERE answer IS NULL ORDER BY createIn ASC LIMIT " + limit + ";")) {
				ArrayList<Ticket> tickets = new ArrayList<Ticket>();
				while (rs.next()) {
					tickets.add(getTicket(rs.getInt("ticketId")));
				}
				return tickets;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			try (ResultSet rs = sql.executeQuery(
					"SELECT ticketId FROM tickets WHERE answer IS NULL ORDER BY createIn DESC LIMIT " + limit + ";")) {
				ArrayList<Ticket> tickets = new ArrayList<Ticket>();
				while (rs.next()) {
					tickets.add(getTicket(rs.getInt("ticketId")));
				}
				return tickets;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public int getTimeInSeconds() {
		return (int) (Calendar.getInstance().getTimeInMillis() / 1000);
	}

	public Date fromSeconds(int seconds) {
		return new Date(new Long((seconds) * 1000L));
	}

}
