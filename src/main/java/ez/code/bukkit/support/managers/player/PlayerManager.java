package ez.code.bukkit.support.managers.player;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class PlayerManager {

	private LoadingCache<Integer, TicketPlayer> cache;
	private LoadingCache<String, Integer> cacheId;

	public PlayerManager() {
		clearCache();
	}

	public void clearCache() {
		cache = CacheBuilder.newBuilder().maximumSize(1000).expireAfterAccess(5, TimeUnit.MINUTES)
				.build(new CacheLoader<Integer, TicketPlayer>() {
					@Override
					public TicketPlayer load(Integer id) throws Exception {
						return new TicketPlayer(id);
					}
				});
		cacheId = CacheBuilder.newBuilder().maximumSize(1000).expireAfterAccess(5, TimeUnit.MINUTES)
				.build(new CacheLoader<String, Integer>() {
					@Override
					public Integer load(String name) throws Exception {
						return new TicketPlayer(name).getId();
					}
				});
	}

	public TicketPlayer getPlayer(int id) {
		try {
			return cache.get(id);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public TicketPlayer getPlayer(String name) {
		try {
			return cache.get(cacheId.get(name));
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;
	}

}
