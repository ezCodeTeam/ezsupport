package ez.code.bukkit.support.managers.ticket.message;

import java.security.InvalidParameterException;

public enum TicketMessageType {
	MESSAGE, STATUSCHANGE, EDIT;

	public byte getValue() {
		return (byte) this.ordinal();
	}

	public static TicketMessageType valueOf(byte b) {
		switch (b) {
		case 0:
			return TicketMessageType.MESSAGE;
		case 1:
			return TicketMessageType.STATUSCHANGE;
		case 2:
			return TicketMessageType.EDIT;
		default:
			throw new InvalidParameterException("Value need to be between 0 and 2");
		}
	}
}
