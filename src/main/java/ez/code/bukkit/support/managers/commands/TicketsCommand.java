package ez.code.bukkit.support.managers.commands;

import static ez.code.bukkit.support.EzSupport.talkToSender;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ez.code.bukkit.core.bukkit.utils.EzCommand;
import ez.code.bukkit.support.EzSupport;
import ez.code.bukkit.support.managers.ticket.Ticket;
import ez.code.bukkit.support.managers.ticket.TicketOrder;
import ez.code.bukkit.support.managers.ticket.TicketStatus;

public class TicketsCommand extends EzCommand {

	public TicketsCommand() {
		super("tickets", // command
				null, // usage
				"Command to manage and answer tickets", // description
				null, // permission
				null, // permission message
				Arrays.asList("support"), // aliases
				EzSupport.getPluginName() // command register. eg:
											// /minecraft:tp (minecraft = register | tp = command)
		);
	}

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String lbl, String[] args) {
		if (s.hasPermission("ezsupport.staff")) {
			if (args.length <= 0) {
				ArrayList<Ticket> tickets = EzSupport.getInstance().getTicketManager().getOpenedTickets(15,
						TicketOrder.OLD);
				if (tickets.size() > 0) {
					talkToSender(s, "&e&lList of 15 more olders tickets:");
					for (Ticket ticket : tickets) {
						talkToSender(
								s, "&6[" + ticket.getTicketId() + "] &eCreated by &6"
										+ ticket.getTicketPlayer().getName() + " &eon &6" + ticket.getCreateInDate(),
								null);
					}
					talkToSender(s, "&e&lEnd of list. Use &o/tickets <id> &e&l to see the ticket.");
				} else {
					talkToSender(s, "Great job! No opened tickets!");
				}
			} else {
				if (args.length > 1) {
					try {
						int id = Integer.parseInt(args[0]);
						if (!EzSupport.getInstance().getTicketManager().getTicket(id).isAnswered()) {
							StringBuilder sb = new StringBuilder();
							for (int i = 1; i < args.length; i++) {
								sb.append(args[i]);
								if (i != (args.length - 1)) {
									sb.append(" ");
								}
							}
							EzSupport.getInstance().getTicketManager().getTicket(id).answer(
									EzSupport.getInstance().getPlayerManager().getPlayer(s.getName()).getId(),
									sb.toString(), false);
							talkToSender(s, "Answered!");
						} else {
							talkToSender(s, "&cThis ticket is closed!");
						}
					} catch (NumberFormatException e) {
						talkToSender(s, "&cUse &o/tickets <id> <answer> &e&l to see the ticket. \"" + args[0]
								+ "\" is not a integer.");
					}
				} else {
					try {
						Ticket ticket = EzSupport.getInstance().getTicketManager().getTicket(Integer.parseInt(args[0]));
						if (ticket.exists()) {
							talkToSender(s, Arrays.asList( //
									"--==[&bTicket " + ticket.getTicketId() + "&7]==--", // HEADER
									"Question: &b" + ticket.getQuestion(), // QUESTION
									"Create by: &b" + ticket.getTicketPlayer().getName(), // CREATE BY
									"Status: &b" + ticket.getTicketStatusFormated(), // STATUS
									(ticket.getTicketStatus() == TicketStatus.CLOSED && ticket.isRated()
											? "Rate: &b" + ticket.getRate().getLevel().toString()
											: null), // RATE
									(ticket.getTicketStatus() == TicketStatus.CLOSED && ticket.hasRateMessage()
											? "Rate Message: &b" + ticket.getRate().getMessage()
											: null), // RATE MESSAGE
									(ticket.isAnswered() ? "Answer: &b" + ticket.getAnswer() : null), // ANSWER
									(ticket.isAnswered() ? "Answer by: &b" + ticket.getTicketStaffer().getName()
											: null), // ANSWER
														// BY
									(ticket.isAnswered() ? "Answer in: &b" + ticket.getAnswerInDate() : null), // ANSWER
																												// IN
									"Created in: &b" + ticket.getCreateInDate(), // CREATE IN
									(ticket.ticketExtends() ? "Extends: &b" + ticket.getExtends() : null), // EXTENDS
									"Views: &b" + (ticket.getViews().size() > 0
											? ticket.getViewsPlayerNames().toString().substring(1,
													ticket.getViewsPlayerNames().toString().length() - 1)
											: "No views.")) // VIEWS
									, "&7");
							if (ticket.getTicketStatus() == TicketStatus.OPENED && s instanceof Player) {
								ticket.addView(
										EzSupport.getInstance().getPlayerManager().getPlayer(s.getName()).getId())
										.reload();
							}
						} else {
							talkToSender(s, "&cThis ticket not exists!");
						}
					} catch (NumberFormatException e) {
						talkToSender(s,
								"&cUse \"/tickets <id>\" to see the ticket. \"" + args[0] + "\" is not a integer.");
					}
				}
			}
		} else

		{
			talkToSender(s, "&cYou don't have permission to do this.");
			return true;
		}
		return false;
	}
}
