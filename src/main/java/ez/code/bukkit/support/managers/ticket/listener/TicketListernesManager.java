package ez.code.bukkit.support.managers.ticket.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TicketListernesManager {

	private List<TicketListener> createTicketListeners = new ArrayList<TicketListener>();
	private List<TicketListener> answerTicketListeners = new ArrayList<TicketListener>();

	public Collection<TicketListener> getCreateTicketListeners() {
		return createTicketListeners;
	}

	public Collection<TicketListener> getAnswerTicketListeners() {
		return answerTicketListeners;
	}

	public void addCreateTicketListener(TicketListener l) {
		createTicketListeners.add(l);
	}

	public void addAnswerTicketListener(TicketListener l) {
		answerTicketListeners.add(l);
	}

}
