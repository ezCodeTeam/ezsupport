package ez.code.bukkit.support.managers.player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ez.code.bukkit.support.EzSupport;
import ez.code.bukkit.support.managers.ticket.Ticket;
import ez.code.core.utils.sql.EzSQL;

public class TicketPlayer {

	private int playerId;
	private String name;

	private final EzSQL sql = EzSupport.getInstance().getSQL();

	public TicketPlayer(int id) {
		this.playerId = id;
		reload();
	}

	public TicketPlayer(String name) {
		this.name = name;
		reload();
	}

	public boolean isRegistred() {
		return name != null && playerId != 0;
	}

	public List<Ticket> getOpenedTickets(int limit) {
		List<Ticket> tickets = new ArrayList<Ticket>();
		try (ResultSet rs = sql.executeQuery("SELECT ticketId FROM tickets WHERE answer IS NULL AND playerId = '"
				+ playerId + "' ORDER BY createIn ASC LIMIT " + limit + ";")) {
			while (rs.next()) {
				tickets.add(EzSupport.getInstance().getTicketManager().getTicket(rs.getInt("ticketId")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tickets;
	}

	public List<Ticket> getPendingTickets(int limit) {
		List<Ticket> tickets = new ArrayList<Ticket>();
		try (ResultSet rs = sql
				.executeQuery("SELECT ticketId FROM tickets WHERE answer IS NOT NULL AND rate = -1 AND playerId = '"
						+ playerId + "' ORDER BY createIn ASC LIMIT " + limit + ";")) {
			while (rs.next()) {
				tickets.add(EzSupport.getInstance().getTicketManager().getTicket(rs.getInt("ticketId")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tickets;
	}

	public boolean hasOpenedTickets() {
		return (getOpenedTickets(1).size() != 0);
	}

	public boolean hasPendingTickets() {
		return (getPendingTickets(1).size() != 0);
	}

	public TicketPlayer reload() {
		if (name != null) {
			try (ResultSet rs = sql.executeQuery("SELECT playerId FROM players WHERE name = '" + name + "';")) {
				if (rs.next()) {
					playerId = rs.getInt("id");
				} else {
					registerNewPlayer();
				}
				// else {
				// throw new NullPointerException("No player found with this name (" + name +
				// ")");
				// }
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else if (new Integer(playerId) != null) {
			try (ResultSet rs = sql.executeQuery("SELECT name FROM players WHERE playerId = " + playerId + ";")) {
				if (rs.next()) {
					name = rs.getString("name");
				} // else {
					// throw new NullPointerException("No player found with this id (" + id + ")");
					// }
					// else {
					// throw new NullPointerException("No player found with this id (" + id + ")");
					// }
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return this;
	}

	public int getId() {
		return playerId;
	}

	private void registerNewPlayer() {
		try {
			sql.executeUpdate("INSERT INTO players (name) values ('" + name + "');");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		reload();
	}

	public String getName() {
		return name;
	}

}
