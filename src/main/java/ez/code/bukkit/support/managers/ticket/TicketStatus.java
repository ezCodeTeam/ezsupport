package ez.code.bukkit.support.managers.ticket;

public enum TicketStatus {
	OPENED, CLOSED, PENDING, SPAM
}
