package ez.code.bukkit.support.managers.ticket;

import java.security.InvalidParameterException;

public class TicketRate {

	private String message;
	private RateLevel level;

	public TicketRate(RateLevel level, String message) {
		this.level = level;
		this.message = message;
	}

	public TicketRate(byte i, String message) {
		this.level = RateLevel.valueOf(i);
		this.message = message;
	}

	public TicketRate(RateLevel level) {
		this.level = level;
	}

	public TicketRate(byte i) {
		this.level = RateLevel.valueOf(i);
	}

	public String getMessage() {
		return message;
	}

	public RateLevel getLevel() {
		return level;
	}

	public enum RateLevel {
		TERRIBLE, // 0
		BAD, // 1
		MEDIUM, // 2
		GOOD, // 3
		EXCELLENT; // 4

		private byte value;

		private RateLevel() {
			value = (byte) this.ordinal();
		}

		public byte getValue() {
			return value;
		}

		public static RateLevel valueOf(byte b) {
			switch (b) {
			case -1:
				return null;
			case 0:
				return TicketRate.RateLevel.TERRIBLE;
			case 1:
				return TicketRate.RateLevel.BAD;
			case 2:
				return TicketRate.RateLevel.MEDIUM;
			case 3:
				return TicketRate.RateLevel.GOOD;
			case 4:
				return TicketRate.RateLevel.EXCELLENT;
			default:
				throw new InvalidParameterException("Value need to be between 0 and 4");
			}
		}
	}

}
