package ez.code.bukkit.support.managers.ticket.message;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import ez.code.bukkit.support.EzSupport;
import ez.code.core.utils.sql.EzSQL;

public class TicketMessage {

	private int messageId;
	private int ticketId;
	private String message;
	private int createIn;
	private int playerId;
	private String server;
	private TicketMessageType messageType;

	private EzSQL sql = EzSupport.getInstance().getSQL();

	public TicketMessage(int messageId) {
		this.messageId = messageId;
		reload();
	}

	public TicketMessage reload() {
		try (ResultSet rs = sql.executeQuery("SELECT * FROM tickets WHERE ticketId = '" + ticketId + "';")) {
			if (rs.next()) {
				ticketId = rs.getInt("ticketId");
				message = rs.getString("message");
				createIn = rs.getInt("createIn");
				playerId = rs.getInt("playerId");
				server = rs.getString("server");
				messageType = TicketMessageType.valueOf(rs.getByte("messageType"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	// GETTERS
	public String getMessage() {
		return message;
	}

	public int getMessageId() {
		return messageId;
	}

	public int getPlayerId() {
		return playerId;
	}

	public int getTicketId() {
		return ticketId;
	}

	public int getCreateIn() {
		return createIn;
	}

	public Date getCreateInDate() {
		return EzSupport.getInstance().getTicketManager().fromSeconds(createIn);
	}

	public TicketMessageType getMessageType() {
		return messageType;
	}

	public String getServer() {
		return server;
	}

	public TicketMessage setServer(String server) {
		try {
			this.server = server;
			sql.executeUpdate("UPDATE messages SET server = '" + server.replace("'", "''") + "' WHERE messageId = '"
					+ messageId + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public TicketMessage setMessage(String message) {
		try {
			this.message = message;
			sql.executeUpdate("UPDATE messages SET message = '" + message.replace("'", "''") + "' WHERE messageId = '"
					+ messageId + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public TicketMessage setPlayerId(int playerId) {
		try {
			this.playerId = playerId;
			sql.executeUpdate(
					"UPDATE messages SET playerId = '" + playerId + "' WHERE messageId = '" + messageId + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public TicketMessage setCreateIn(int createIn) {
		try {
			this.createIn = createIn;
			sql.executeUpdate(
					"UPDATE messages SET createIn = '" + createIn + "' WHERE messageId = '" + messageId + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public TicketMessage setMessageType(TicketMessageType messageType) {
		try {
			this.messageType = messageType;
			sql.executeUpdate("UPDATE messages SET messageType = '" + messageType.getValue() + "' WHERE messageId = '"
					+ messageId + "';");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

}
