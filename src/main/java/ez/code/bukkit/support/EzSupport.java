package ez.code.bukkit.support;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import ez.code.bukkit.core.bukkit.utils.EzChat;
import ez.code.bukkit.support.listeners.tickets.TicketAnswered;
import ez.code.bukkit.support.listeners.tickets.TicketCreated;
import ez.code.bukkit.support.managers.commands.CommandsManager;
import ez.code.bukkit.support.managers.player.PlayerManager;
import ez.code.bukkit.support.managers.ticket.TicketManager;
import ez.code.core.utils.EzVersionManager;
import ez.code.core.utils.sql.EzColumn;
import ez.code.core.utils.sql.EzSQL;
import ez.code.core.utils.sql.EzTable;

public class EzSupport extends JavaPlugin {

	private EzVersionManager versionManager;
	private ConsoleCommandSender console;
	private EzSQL sql;
	private TicketManager ticketManager;
	// STATICS
	private static EzSupport instance;
	private static String pluginName;
	private static String prefix = "&7[&bEzSupport&7] &b";
	private PlayerManager playerManager;

	@Override
	public void onEnable() {
		initInstance();
		initConsoleSender();
		initVersionManager();
		talkToConsole("Connecting to Database...");
		initDatabase();
		talkToConsole("Done");
		initManagers();
		initListeners();
		initCommands();
	}

	@Override
	public void onDisable() {
		talkToConsole("Disconnecting from Database...");
		stopDatabase();
	}

	public void stopDatabase() {
		try {
			sql.disconnect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void initListeners() {
		getTicketManager().getListernersManager().addCreateTicketListener(new TicketCreated());
		getTicketManager().getListernersManager().addAnswerTicketListener(new TicketAnswered());
	}

	public void initCommands() {
		CommandsManager.register();
	}

	private void initInstance() {
		instance = this;
	}

	public void initManagers() {
		ticketManager = new TicketManager();
		playerManager = new PlayerManager();
	}

	public void initDatabase() {
		try {
			if (!getDataFolder().exists())
				getDataFolder().mkdir();
			sql = new EzSQL(getDataFolder() + File.separator + "db.sqlite", "ezsupport");
			sql.connect();
			sql.createTable(new EzTable("players") // `players` table
					.addColumn(new EzColumn("playerId", "INTEGER", "PRIMARY KEY AUTOINCREMENT")) // id of player
					.addColumn(new EzColumn("name", "VARCHAR(16)", "NOT NULL UNIQUE")) // name of player
			// FIXME .addColumn(new EzColumn("uuid", "BINARY(16)", "UNIQUE")) // uuid of
			// player
			);
			sql.createTable(new EzTable("messages") // `messages` table
					.addColumn(new EzColumn("messageId", "INTEGER", "PRIMARY KEY AUTOINCREMENT")) // id of message
					.addColumn(new EzColumn("ticketId", "INTEGER", "NOT NULL")) // id of ticket
					.addColumn(new EzColumn("playerId", "INTEGER", "NOT NULL")) // id of player
					.addColumn(new EzColumn("server", "VARCHAR(30)", "NOT NULL")) // name of server
					.addColumn(new EzColumn("message", "VARCHAR(200)", "NOT NULL")) // the message
					.addColumn(new EzColumn("createIn", "INTEGER", "NOT NULL")) // when message was created
					.addColumn(new EzColumn("messageType", "TINYINT", "NOT NULL, DEFAULT 0")) // type of message
			);
			sql.createTable(new EzTable("views") // `viewed` table
					.addColumn(new EzColumn("ticketId", "INTEGER", "NOT NULL")) // id of the ticket
					.addColumn(new EzColumn("playerId", "INTEGER", "NOT NULL, "// id if player
							+ "PRIMARY KEY(playerId, ticketId)")) //
			);
			sql.createTable(new EzTable("tickets") // `tickets` table
					.addColumn(new EzColumn("ticketId", "INTEGER", "PRIMARY KEY AUTOINCREMENT")) // id of ticket
					.addColumn(new EzColumn("playerId", "INTEGER", "NOT NULL")) // id of player
					.addColumn(new EzColumn("question", "VARCHAR(200)", "NOT NULL")) // question of ticket
					.addColumn(new EzColumn("stafferId", "INTEGER")) // id of staffer who answered the ticket
					.addColumn(new EzColumn("answer", "VARCHAR(200)")) // answer of the ticket
					.addColumn(new EzColumn("answerIn", "INTEGER")) // when answer was created
					.addColumn(new EzColumn("rate", "TINYINT", "DEFAULT -1")) // rate of answer
					.addColumn(new EzColumn("rateMessage", "VARCHAR(100)")) // rate message of answer
			);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	private void initConsoleSender() {
		console = Bukkit.getConsoleSender();
	}

	private void initVersionManager() {
		versionManager = new EzVersionManager(getDescription().getVersion());
	}

	// GETTERS

	public static ConsoleCommandSender getConsole() {
		return getInstance().getConsoleSender();
	}

	public ConsoleCommandSender getConsoleSender() {
		return console;
	}

	public EzVersionManager getVersionManager() {
		pluginName = getDescription().getName();
		return versionManager;
	}

	public EzSQL getSQL() {
		return sql;
	}

	public static String getPrefix() {
		return prefix;
	}

	public static void setPrefix(String prefix) {
		EzSupport.prefix = prefix;
	}

	public static String getPluginName() {
		return pluginName;
	}

	public static EzSupport getInstance() {
		return instance;
	}

	public static void talkToSender(CommandSender s, String message) {
		s.sendMessage(EzChat.colorMessage(prefix + message));
	}

	public static void talkToSender(CommandSender s, String[] messages) {
		for (String message : messages) {
			if (message != null)
				s.sendMessage(EzChat.colorMessage(prefix + message));
		}
	}

	public static void talkToSender(CommandSender s, String[] messages, String prefix) {
		for (String message : messages) {
			if (prefix == null && message != null)
				s.sendMessage(EzChat.colorMessage(message));
			else if (message != null)
				s.sendMessage(EzChat.colorMessage(prefix + message));
		}
	}

	public static void talkToSender(CommandSender s, List<String> messages, String prefix) {
		for (String message : messages) {
			if (prefix == null && message != null)
				s.sendMessage(EzChat.colorMessage(message));
			else if (message != null)
				s.sendMessage(EzChat.colorMessage(prefix + message));
		}
	}

	public static void talkToSender(CommandSender s, String message, String prefix) {
		if (prefix == null)
			s.sendMessage(EzChat.colorMessage(message));
		else
			s.sendMessage(EzChat.colorMessage(prefix + message));
	}

	public static void talkToConsole(String message) {
		getConsole().sendMessage(EzChat.colorMessage(prefix + message));
	}

	public TicketManager getTicketManager() {
		return ticketManager;
	}

	public PlayerManager getPlayerManager() {
		return playerManager;
	}

}
